import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import Body from './components/body';
import ToobibPage from './components/toobibPage';
import error from './components/error';


const urlAPI = 'https://annuaire.interhop.org/providers?search='
const urlAPIToobib = 'https://annuaire.interhop.org/provider/'

export default class App extends Component {
  	state = {
		'searchValue': 'Paris',
		'lastSearchTitle': '',
		'searchResult': '',
		'page': '1',
		'toobibId': '3054146',
		'toobibData': null,
		'setToobibData': null,
		'loading': false,
		'setLoading':false
	}

	async toobibDataFetch() {
   		const response = await fetch(urlAPIToobib + this.state.toobibId, { header: {'Access-Control-Allow-Origin': '*'}})
		const json = await response.json();
		this.setState({ 'toobibData': json });
	}
	
	toobibDataFunction = () => e => {
		this.toobibDataFetch();
	}


	async fetchData() {
   		const response = await fetch(urlAPI + this.state.searchValue + '&page=' + this.state.page + '&origin=*', { header: {'Access-Control-Allow-Origin': '*'}})
		const json = await response.json();
		this.setState({ 'searchResult': json });
	}

	resetSearch() {
		this.setState({'page': '1'});
		this.setState({'lastSearchTitle': this.state.searchValue});
	}

	searchFunction = () => e => {
		this.resetSearch();
		e.preventDefault();
		this.fetchData();
	}

	handleChange = input => e => {
		this.setState({[input]: e.target.value});
	}

	handleIndentation = input => async e => {
		let newNumber = parseInt(e.target.value) + parseInt(this.state.page);
		await this.setState({[input]: newNumber.toString()});
		this.fetchData();
	}
	
	render() {
		const { searchValue, searchResult, lastSearchTitle, page } = this.state;
		return (
		 <Router>
			<Switch>
	 			<Route exact path="/" render={(props) => <Body {...props} fetchData={this.fetchData} searchResult={searchResult} page={page} toobibDataFunction={this.toobibDataFunction} handleIndentation={this.handleIndentation} lastSearchTitle={lastSearchTitle} searchResult={searchResult} handleChange={this.handleChange} searchValue={searchValue} searchFunction={this.searchFunction} /> } />
				<Route path="/toobibPage/:Id" render={(props) => <ToobibPage {...props} toobibData={this.state.toobibData} /> } />
	  			<Route exact path="/404" component={error} />
	  			<Redirect to="/404" />
	  		</Switch>
	  	</Router>
  		);
	}
}

