import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Header from './header';


	function ProfileDetails(resources) {
		return (
			<div>
				<Header />
				<div className="container column is-10">
					<div className="card toobib_data">
						<div className="IMG_container">
							<img className="IMG_container__img--doctor"/>
						</div>
						<h3 className="subtitle toobib_resources__title">{resources.data.prenom}  {resources.data.nom}</h3>
						<h4 className="toobib_resources__position">{resources.data.libelleProfession}</h4>
						<p className="toobib_resources__number">téléphone: {resources.data.telephone}</p>
						<p className="toobib_resources__email">e-mail: {resources.data.email}</p>
						<p className="toobib_resources__convention">Conventionnement: null {resources.data.conventionnement}</p>
					</div>
					<br/>
					<div className="card location_data">
						<h3 className="subtitle location_data__title">Adresse</h3>
						<p className="location_data__address">NOM DU LIEU</p>
						<p className="location_data__address">XX rue TEST TEST, {resources.data.libelleCommune}</p>
						<br/>
						<h3 className="subtitle location_data__title">Informations supplémentaires</h3>
						<p className="location_data__info">TEST TEST INFO SUP</p>
						<p className="location_data__info">TEST INFO HANDICAPE</p>
						<p className="location_data__info">TEST INFO PARKING </p>
						<div className="MAP_container">
							<img className="MAP_container__map--doctor"/>
						</div>
					</div>
				</div>
			</div>
		)
	}


const ToobibPage = (props) =>  {
	const [toobibData, setToobibData] = useState(null);

	const toobibDataFunction = async () => {
		try {
		const data = await axios
			.get('https://annuaire.interhop.org/provider/' + props.match.params.Id)
			.then(res => {
				console.log(res);
				setToobibData(JSON.stringify(res.data));
			});
		} catch(e) {
			console.log(e);
		}
	}
	function stringToJSON(data) {
		let result = (data + '').split(',');
		let json = {}; 
		result.forEach((item) => { 
			let pair = item.split(':'); 
			pair[0] = pair[0].split('\"')[1];
			if(typeof(pair[1]) === "string")

				if(pair[1].split('\"')[0] === '') 
					pair[1] = pair[1].split('\"')[1];
				else
					pair[1] = pair[1].split('\"')[0];
			json[pair[0]] = pair[1];
			});
		result = json;
		return (result);
	}

	useEffect(() => {
		toobibDataFunction();
	}, []);
	
		return (
			<div>
				{toobibData ? <ProfileDetails data={stringToJSON(toobibData)} /> : <progress class="progress is-small is-primary" max="100">15%</progress>}
			</div>
		);
	}

export default ToobibPage;

