import React, { Component } from 'react';
import SearchBar from './searchbar';
import Header from './header';
import ResultCards from './result-cards';
import Pagination from './pagination';


// URL API adrien 
 const urlAPI = 'https://annuaire.interhop.org/providers?search='

export default function Body(props) {
	
		const { searchValue, searchResult, lastSearchTitle, page,fetchData, toobibDataFunction, handleChange, searchFunction, handleIndentation } = props;
	return (
   		<main>
      			<Header/>
			<SearchBar fetchData={fetchData} handleChange={handleChange} searchValue={searchValue} searchFunction={searchFunction} />
    			<ResultCards lastSearchTitle={lastSearchTitle} searchResult={searchResult} toobibDataFunction={toobibDataFunction}/>
			<Pagination searchResult={searchResult} page={page} handleIndentation={handleIndentation} />
		</main>
  );
	}
