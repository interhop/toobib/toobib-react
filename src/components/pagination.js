import React, { Component } from 'react';

export default class Pagination extends Component {

	render() {

		const { handleIndentation, page, searchResult } = this.props;
/*		if(!searchResult.providers[0]) {
			return(
				<nav aria-label="Pagination" className="columns is-centered">
				<button value="-1" onClick={handleIndentation("page")} className="pagination-previous" disabled>
				<span className="screen-reader-text">Page précédente</span>
				<svg className="icon icon-arrow-left-mini" width="14" height="14" viewBox="-100.9 99.1 61.9 105.9">
				<path d="M-98.2 158.8l43.5 43.5c1.7 1.7 4 2.7 6.5 2.7s4.8-1 6.5-2.7c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.7-6.5l-37.2-37.2 37.2-37.2c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.6-6.5c-1.8-1.9-4.2-2.8-6.6-2.8-2.3 0-4.6.9-6.5 2.6-11.5 11.4-41.2 41-43.3 43l-.2.2c-3.6 3.6-3.6 10.3 0 13.9z"></path>			
				</svg>
				Précédente
				</button>
				<button value="1" onClick={handleIndentation("page")} className="pagination-next" disabled>

				<span className="screen-reader-text">Page suivante</span>
				Suivante
				<svg width="14" height="14" viewBox="-100.9 99.1 61.9 105.9" className=" icon icon-arrow-right-mini">
				<path d="M-41.7 145.3l-43.5-43.5c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.7c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.7 6.5L-61 152l-37.2 37.2c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.6 6.5c1.8 1.9 4.2 2.8 6.6 2.8 2.3 0 4.6-.9 6.5-2.6 11.5-11.4 41.2-41 43.3-43l.2-.2c3.6-3.6 3.6-10.4 0-13.9z"></path>
				</svg>
				</button>
				</nav>
			)
		} else */ if(page === '1' && (!searchResult.providers || !searchResult.providers[0])) {
			return(
				<nav aria-label="Pagination" className="columns is-centered">
				<button value="-1" onClick={handleIndentation("page")} className="pagination-previous" disabled>
				<span className="screen-reader-text">Page précédente</span>
				<svg className="icon icon-arrow-left-mini" width="14" height="14" viewBox="-100.9 99.1 61.9 105.9">
				<path d="M-98.2 158.8l43.5 43.5c1.7 1.7 4 2.7 6.5 2.7s4.8-1 6.5-2.7c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.7-6.5l-37.2-37.2 37.2-37.2c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.6-6.5c-1.8-1.9-4.2-2.8-6.6-2.8-2.3 0-4.6.9-6.5 2.6-11.5 11.4-41.2 41-43.3 43l-.2.2c-3.6 3.6-3.6 10.3 0 13.9z"></path>			
				</svg>
				Précédente
				</button>
				<button value="1" onClick={handleIndentation("page")} className="pagination-next" disabled>

				<span className="screen-reader-text">Page suivante</span>
				Suivante
				<svg width="14" height="14" viewBox="-100.9 99.1 61.9 105.9" className=" icon icon-arrow-right-mini">
				<path d="M-41.7 145.3l-43.5-43.5c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.7c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.7 6.5L-61 152l-37.2 37.2c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.6 6.5c1.8 1.9 4.2 2.8 6.6 2.8 2.3 0 4.6-.9 6.5-2.6 11.5-11.4 41.2-41 43.3-43l.2-.2c3.6-3.6 3.6-10.4 0-13.9z"></path>
				</svg>
				</button>
				</nav>
			)
		} else if(page === '1') {
			return(
				<nav aria-label="Pagination" className="columns is-centered">
				<button value="-1" onClick={handleIndentation("page")} className="pagination-previous" disabled>
				<span className="screen-reader-text">Page précédente</span>
				<svg className="icon icon-arrow-left-mini" width="14" height="14" viewBox="-100.9 99.1 61.9 105.9">
				<path d="M-98.2 158.8l43.5 43.5c1.7 1.7 4 2.7 6.5 2.7s4.8-1 6.5-2.7c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.7-6.5l-37.2-37.2 37.2-37.2c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.6-6.5c-1.8-1.9-4.2-2.8-6.6-2.8-2.3 0-4.6.9-6.5 2.6-11.5 11.4-41.2 41-43.3 43l-.2.2c-3.6 3.6-3.6 10.3 0 13.9z"></path>			
				</svg>
				Précédente
				</button>
				<button value="1" onClick={handleIndentation("page")} className="pagination-next">

				<span className="screen-reader-text">Page suivante</span>
				Suivante
				<svg width="14" height="14" viewBox="-100.9 99.1 61.9 105.9" className=" icon icon-arrow-right-mini">
				<path d="M-41.7 145.3l-43.5-43.5c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.7c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.7 6.5L-61 152l-37.2 37.2c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.6 6.5c1.8 1.9 4.2 2.8 6.6 2.8 2.3 0 4.6-.9 6.5-2.6 11.5-11.4 41.2-41 43.3-43l.2-.2c3.6-3.6 3.6-10.4 0-13.9z"></path>
				</svg>
				</button>
				</nav>
			)
		} else if(searchResult.providers.length < 9) {
			return(
				<nav aria-label="Pagination" className="columns is-centered">
				<button value="-1" onClick={handleIndentation("page")} className="pagination-previous">
				<span className="screen-reader-text">Page précédente</span>
				<svg className="icon icon-arrow-left-mini" width="14" height="14" viewBox="-100.9 99.1 61.9 105.9">
				<path d="M-98.2 158.8l43.5 43.5c1.7 1.7 4 2.7 6.5 2.7s4.8-1 6.5-2.7c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.7-6.5l-37.2-37.2 37.2-37.2c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.6-6.5c-1.8-1.9-4.2-2.8-6.6-2.8-2.3 0-4.6.9-6.5 2.6-11.5 11.4-41.2 41-43.3 43l-.2.2c-3.6 3.6-3.6 10.3 0 13.9z"></path>			
				</svg>
				Précédente
				</button>
				<button value="1" onClick={handleIndentation("page")} className="pagination-next" disabled>

				<span className="screen-reader-text">Page suivante</span>
				Suivante
				<svg width="14" height="14" viewBox="-100.9 99.1 61.9 105.9" className=" icon icon-arrow-right-mini">
				<path d="M-41.7 145.3l-43.5-43.5c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.7c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.7 6.5L-61 152l-37.2 37.2c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.6 6.5c1.8 1.9 4.2 2.8 6.6 2.8 2.3 0 4.6-.9 6.5-2.6 11.5-11.4 41.2-41 43.3-43l.2-.2c3.6-3.6 3.6-10.4 0-13.9z"></path>
				</svg>
				</button>
				</nav>
			)
		} else {
			return(
				<nav aria-label="Pagination" className="columns is-centered">
				<button value="-1" onClick={handleIndentation("page")} className="pagination-previous">
				<span className="screen-reader-text">Page précédente</span>
				<svg className="icon icon-arrow-left-mini" width="14" height="14" viewBox="-100.9 99.1 61.9 105.9">
				<path d="M-98.2 158.8l43.5 43.5c1.7 1.7 4 2.7 6.5 2.7s4.8-1 6.5-2.7c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.7-6.5l-37.2-37.2 37.2-37.2c1.7-1.7 2.7-4 2.7-6.5s-1-4.8-2.6-6.5c-1.8-1.9-4.2-2.8-6.6-2.8-2.3 0-4.6.9-6.5 2.6-11.5 11.4-41.2 41-43.3 43l-.2.2c-3.6 3.6-3.6 10.3 0 13.9z"></path>			
				</svg>
				Précédente
				</button>
				<button value="1" onClick={handleIndentation("page")} className="pagination-next">

				<span className="screen-reader-text">Page suivante</span>
				Suivante
				<svg width="14" height="14" viewBox="-100.9 99.1 61.9 105.9" className=" icon icon-arrow-right-mini">
				<path d="M-41.7 145.3l-43.5-43.5c-1.7-1.7-4-2.7-6.5-2.7s-4.8 1-6.5 2.7c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.7 6.5L-61 152l-37.2 37.2c-1.7 1.7-2.7 4-2.7 6.5s1 4.8 2.6 6.5c1.8 1.9 4.2 2.8 6.6 2.8 2.3 0 4.6-.9 6.5-2.6 11.5-11.4 41.2-41 43.3-43l.2-.2c3.6-3.6 3.6-10.4 0-13.9z"></path>
				</svg>
				</button>
				</nav>
			)
		}
	}
}
