import React, { Component } from 'react';

export default class SearchBar extends Component {
		
	render() {
		const { searchValue, handleChange, searchFunction, fetchData } = this.props;
		return(
			<React.Fragment>
			<div className="level">
			<div className="level-item">
				<form action="" method="GET">
					<div className="field">
						<div className="control">
							<input className="input" value={searchValue} onChange={handleChange('searchValue')} placeholder="search" name="q" type="text"/>
						</div>
						<div className="control">
							<input type="submit" className="button is-link" onClick={searchFunction()} value="Rechercher"></input>
						</div>
					</div>
				</form>
			</div>
		</div>
			</React.Fragment>
		)
	}
}
