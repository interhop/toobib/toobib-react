import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';

export default class ResultCards extends Component {

	render() {
		const { searchResult, lastSearchTitle, toobibDataFunction } = this.props;
		if(!searchResult)
		{
			return(
				<React.Fragment>
					<h2>faites une recherche !</h2>
				</React.Fragment>
			)
		}
		else if(!searchResult.providers[0]) {
			return(
				<React.Fragment>
					<h2>Il n'y a pas de résultat pour la recherche: "{lastSearchTitle}"</h2>
				</React.Fragment>
			)
		} 
		else 
		{

//			let e = searchResult.providers[0];
			return(
				<React.Fragment>
				<h2>Voici les resultats pour: "{lastSearchTitle}"</h2>
						<nav className="columns is-multiline is-centered">
				{
					searchResult.providers.map((e) => {
						return(

							<div className="column is-one-quarter has-text-centered">
							<div className="card">
								<div className="card-content">
									<div className="container">
										<div className="media-content">
											<p className="title is-4">{e.prenom} {e.nom}</p>
											<b>
												<p className="is-bold is-4">{e.libelleCommune}</p>
											</b>
										</div>
									</div>
									<div className="content">
										{e.libelleProfession}
										<br />
									</div>
									<Link to={"/toobibPage/" + e.id} className="button is-link is-light has-text-right" value={e.id}>Consulter</Link>
								</div>
							</div>
							</div>
					)})
				}
				</nav>
				</React.Fragment>
			)
		}
	}
}
