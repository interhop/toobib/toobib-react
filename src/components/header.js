import React, { Component } from 'react';

export default class header extends Component {

	render() {
		return(
			<React.Fragment>
				<section className="hero is-small is-info">
					<div className="hero-body has-text-centered">
						<div className="container">
							<h1 className="title">
								Annuaire
							</h1>
							<h2 className="subtitle">
								Trouvez votre practicien-ne
							</h2>
						</div>
					</div>
				</section>
			</React.Fragment>
		)
	}
}
